FROM phpunit/phpunit:5.0.3
MAINTAINER Cristi Contiu <cristian.contiu@bixel.ro>
ENV SYMFONY_ENV test
COPY composer.json /composer-cache/composer.json
COPY composer.lock /composer-cache/composer.lock
RUN cd /composer-cache && composer install --no-interaction --no-scripts --no-autoloader
RUN rm -rf /composer-cache