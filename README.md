Using Bitbucket Pipelines with Symfony2
=======================================

This demo integrates the [Symfony Demo Application](https://github.com/symfony/symfony-demo) in a continuous integration workflow using Bitbucket Pipelines. It runs the machine from a [phpunit Docker image](https://hub.docker.com/r/phpunit/phpunit/) and and builds & tests the app using composer and phpunit.

Enable Pipelines
----------------
To enable Bitbucket Pipelines on your repository, click the "Pipelines" menu item on the left and follow the instructions. A few edits to the default `bitbucket-pipelines.yml` file are required for a Symfony2 project :

  1. Set the SYMFONY_ENV enviorment variable to "test". Can also be done in Settings > Pipelines > Environment Variables. 
  2. Run `composer install` without interactions or prompts.
  3. Run phpunit.
 

This is a working example of my `bitbucket-pipelines.yml` file:

```
#!yaml
image: phpunit/phpunit:5.0.3

pipelines:
  default:
    - step:
        script:
          - export SYMFONY_ENV=test
          - composer install --no-interaction
          - phpunit || ( echo "Tests failed! Dumping logs:" && cat var/logs/test.log && exit 1 )
```

**NOTE**: You should choose an image identical or as close as possible to your production environment.


Speeding up the tests
---------------------
Everything is working properly, but the builds are pretty slow: even for a few tests, it takes 1:30 - 2:30 minutes before you get feedback. Most of the time it is spent waiting for `composer` to download the dependencies. As a new instance is created for every commit or pull-request, `composer` cannot cache the packages.

A solution would be to use a custom Docker image for which the `composer install` command was run during the build process so the dependencies are cached. For this, we will need to:

  1. Create a Dockerfile and build a local image
  2. Publish the image to [Docker Hub](https://hub.docker.com/)
  3. Use the custom image in the `bitbucket-pipelines.yml`


This is the `Dockerfile` I used in this project:

```bash
FROM phpunit/phpunit:5.0.3
MAINTAINER Cristi Contiu <cristian.contiu@bixel.ro>
ENV SYMFONY_ENV test
COPY composer.json /composer-cache/composer.json
COPY composer.lock /composer-cache/composer.lock
RUN cd /composer-cache && composer install --no-interaction --no-scripts --no-autoloader
RUN rm -rf /composer-cache
```

To build a local image and publish it on Docker Hub, run:

```bash
$ cd /your/project/folder/
$ docker build -t <your-dockerhub-username>/bitbucket-pipelines-symfony .
$ docker push <your-dockerhub-username>/bitbucket-pipelines-symfony
```

Then, change the image used in `bitbucket-pipelines.yml` from `phpunit/phpunit:5.0.3` to `<your-dockerhub-username>/bitbucket-pipelines-symfony`:

```
#!yaml
image:  <your-dockerhub-username>/bitbucket-pipelines-symfony

pipelines:
  default:
    - step:
        script:
          - export SYMFONY_ENV=test
          - composer install --no-interaction
          - phpunit || ( echo "Tests failed! Dumping logs:" && cat var/logs/test.log && exit 1 )
```


After these changes, the tests run in 15 - 30 seconds. 

**CAVEATS**:

  1. For every `composer update` you should build & push the Docker image. Otherwise, not all packages will be cached.
  2. If you work on a **private project**, you should probably use a private Docker image.


Output debug logs in case of tests failure
------------------------------------------
If the tests fail, it would be very useful to output the test logs. We can do this by running the `cat var/logs/test.log` command only if the phpunit command fails and also provide an non-zero exit code to keep the failed status. The full command will be:
```
phpunit || ( echo "Tests failed! Dumping logs:" && cat var/logs/test.log && exit 1 )
```